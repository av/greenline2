# Greenline World

___
The Greenline World is a Gazebo world created for this class as a testbed for line following.  

![Greenline World](.Images/greenline2.jpg)

# Installation

Clone this repo into your workspace `ros_ws/src` folder.  
```
R:~/av/ros_ws/src$ git clone https://gitlab.msu.edu/av/greenline2.git
```
Then build it from the `ros_ws` folder with:
```
R:~/av/ros_ws$ colcon build --packages-select greenline2 --symlink-install
```
And finally, source your overlay:
```
R:~/av/ros_ws$ source install/setup.bash
```

# Start the World

While you can run this world with the `burger` Turtlebot, this is not ideal as it does not have a camera.  Instead use the `waffle` Turtlebot.  On HPCC you should be able to switch to a `waffle` Turtlebot with the command:
```
R:~$ tbot waffle
export TURTLEBOT3_MODEL=waffle
```
Once you have set the Turtlebot, run the world in Gazebo with:
```
R:~$ ros2 launch greenline2 greenline2.launch.py
```

# Control the Turtlebot

You can control the Turtlebot the same way as usual, namely by publishing a `Twist` to the `/cmd_vel` topic.  For example:
```
R:~$ ros2 topic pub /cmd_vel geometry_msgs/msg/Twist '{linear: {x: 0.2}, angular: {z: -0.2}}' -1
```
Stop the Turtlebot with an all-zero Twist, namely:
```
R:~$ ros2 topic pub /cmd_vel geometry_msgs/msg/Twist '{}' -1
```
Alternatively, you can teleoperate the Turtlebot with the command:
```
R:~$ ros2 run teleop_twist_keyboard teleop_twist_keyboard
```
# Reset the Turtlebot

To reset the Turtlebot to its starting location, simply select `Edit / Reset Model Poses` from the Gazebo menu.

# Image Topic

Gazebo shows a Turtlebot-centric view of the camera image.  But you can also directly view the image using `rqt`.  Simply type:
```
R:~$ rqt
```
Then select `Plugins / Visualization / Image View`, and under the image topic select: `/camera/image_raw/compressed`.  You should see something like this:

![Image View](.Images/image_view.jpg)

